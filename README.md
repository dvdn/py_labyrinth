# py_labyrinth

Labyrinth minimalist game written in Python (for learning purpose).
Several maps avalaible. You can also create your own maps

## Rules of the game

Find your way around a map to exit ('U' sign)
- to move : direction (n, s, e or o) + steps number
- spells on doors or walls : 'm' to wall up, 'p' to pierce + direction (n, s, e or o)


## Changelog

version 1.2.1
- magic spells
- automated tests on map

version 1.2.0
- random start position on map without preset
- new map addition

version 1.1.0
- move multiple steps allowed
- new map addition

version 1.0.0
- labyrinth game in terminal

